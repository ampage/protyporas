## Protyporas

A C library of type-agnostic data structures, written from scratch.
Intended to replace my (largely incomplete) individual data structure projects.

NOTE: It's very unfinished, I just put it here for people to look at if interested.

# General notes

Should provide a single header file which re-exports (well, the C equivalent)
the definitions exposed by the header files for each of the data structures'
implementations. Could have internal modules to minimize code re-use where
possible (and possibly to decouple things from the library's external API).

Use include guards where necessary.

Find out if defining a struct type in a header does allow clients to
access its fields directly. Just declaring them in the header causes
complaints, but that is good. Shouldn't violate decoupling.)

Refer to the opendatastructures.org book (and Conor's notes) for ideas and
formal discussion.

# Data structures to provide first (the ones needed for Dipsl experiments):

Singly-linked list supporting stack, fifo-queue and functional list operations.
(Store pointer to last element for O(1) append and prepend).

My unordered array list - "simplist" (others can wait).

Array stacks (since I've already got one).

Binary heap (array-based, if I can figure that out, or crappier homebrew one).

# Ones to provide later:

More array lists (my ordered "compacting" one ("squist"), and/or any better
ones from the book.)

Doubly-linked lists.

AVL trees.

FIFO queue - array implementation from the book.

# Files

tuples.h
fail.{h,c} (internal)
(resize-array.{h,c} (internal, example, undecided))

sl-list.{h,c}
simplist.{h,c}
ar-stack.{h,c}
(bin-heap.{h,c})

protyporas.h (no .c file needed, right? Does it need to explicitly re-export
declarations, or can it be done implicitly?)
