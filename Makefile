# This Makefile goes in project root directory. Pathnames are relative to this.

# For printing, see below
VARS_OLD := $(.VARIABLES)

LIBNAME = libprotyporas
SRCDIR = src
BINDIR = bin
TESTDIR = test
TESTS = $(wildcard $(TESTDIR)/*.c)
TESTBIN = $(BINDIR)/run-tests
# TESTBINS = $(addprefix $(BINDIR)/,$(basename $(notdir TESTS)))
SOURCES = $(wildcard $(SRCDIR)/*.c)

# For now, both my .h and .c files go in SRCDIR.

CC = gcc
CFLAGS = -std=c11 -pedantic -Wall -I$(SRCDIR)
TESTFLAGS = -O0 -g
STATICFLAGS = -O3 -c
SHAREDFLAGS = -O3 -shared -fPIC
# LDFLAGS = linker options - add if using other libraries

# Print the values of all my variables
# $(foreach v, \
#   $(filter-out $(VARS_OLD) VARS_OLD,$(.VARIABLES)), \
#   $(info $(v) = $($(v))))

# Add this to a rule for debugging
# $(info dollar-chevron is $^)

# Special Make macros

# $@ name of current makefile rule.
# $? names of dependents that have changed (i.e. that need recompilation)
# $^ filenames of all prerequisites for the rule's dependency/ies
# $* prefix shared by target and dependent files
# $< the name of the related file that caused the current action

all: compile-tests shared static

compile-tests: $(SOURCES) $(TESTS)
	$(CC) $(CFLAGS) $(TESTFLAGS) $^ -o $(TESTBIN)

run-tests: compile-tests
	LD_LIBRARY_PATH=. $(TESTBIN)

vrun-tests: compile-tests
	LD_LIBRARY_PATH=. valgrind --tool=memcheck --leak-check=yes \
        --track-origins=yes $(TESTBIN)

# Why do I get "Must remake target 'shared' and 'static'" even after all their
# dependencies have been pruned? And do I need to separate the two steps of the
# static rule to prevent unnecessary recompilation?

shared: $(SOURCES)
	$(CC) $(CFLAGS) $(SHAREDFLAGS) $^ -o $(BINDIR)/$(LIBNAME).so

# Must compile each .c file to an object file individually, then "partially
# link" those into another object file which can then be statically linked.

# Note the vital semicolon: foreach expands the text in its body several times
# (each time making different a substitution); in this case, another separate
# invocation of gcc is appended each time. The semicolon ensures that these
# commands are interpreted separately, and not as one long (invalid) command.

# This would be easier to read if I could accumulate a list of the inter-
# mediate file names, rather than having to generate them again in the ld
# command. Is this possible?

static: $(SOURCES)
	$(foreach v, $^, \
	$(CC) $(CFLAGS) $(STATICFLAGS) $(v) -o \
	$(BINDIR)/$(basename $(notdir $(v))).o ;)
	ld -o $(BINDIR)/$(LIBNAME).o -r \
	$(addsuffix .o,$(addprefix $(BINDIR)/,$(basename $(notdir $^))))

# The preceding hyphen prevents rule failure even if rm fails

clean:
	-rm -f $(BINDIR)/*

# Below is a "pattern rule" - could adapt this to allow test suites to be run
# individually without commenting anything out?

# examples/%: examples/%.c mpc.c
#	$(CC) $(CFLAGS) $^ -lm -o $@
