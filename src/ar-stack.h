
#ifndef AR_STACK_H_INCLUDED
#define AR_STACK_H_INCLUDED

#include <stdbool.h>

typedef void* ar_stack_item;

struct ar_stack;
typedef struct ar_stack ar_stack;

ar_stack *make_ar_stack(int initial_capacity);

void free_ar_stack(ar_stack *stack);

bool ar_stack_empty(ar_stack *stack);

void ar_stack_push(ar_stack *stack, ar_stack_item item);

ar_stack_item ar_stack_pop(ar_stack *stack);

ar_stack_item ar_stack_peek(ar_stack *stack);

#endif
