
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "ar-stack.h"

struct ar_stack {
  int num_items;
  int capacity;
  ar_stack_item *items;
};

// Initial capacity must be of reasonable size relative to void pointer size.
ar_stack *make_ar_stack(int initial_capacity) {
  ar_stack *stack = malloc(sizeof(ar_stack));
  stack->num_items = 0;
  stack->capacity = initial_capacity;
  stack->items = malloc(stack->capacity * sizeof(ar_stack_item));
  return stack;
}

void free_ar_stack(ar_stack *stack) {
  free(stack->items);
  free(stack);
}

static void extend_stack(ar_stack *stack) {
  stack->capacity = stack->capacity * 3 / 2;
  ar_stack_item *temp = realloc(stack->items, stack->capacity * sizeof(ar_stack_item));
  if (temp == NULL) {
    fprintf(stderr, "Realloc failed.\n");
    exit(1);
  }
  stack->items = temp;
}

bool ar_stack_empty(ar_stack *stack) {
  return (stack->num_items == 0);
}

static bool full(ar_stack *stack) {
  return (stack->num_items == stack->capacity);
}

void ar_stack_push(ar_stack *stack, ar_stack_item item) {
  stack->items[stack->num_items] = item;
  stack->num_items++;
  if (full(stack)) extend_stack(stack);
}

ar_stack_item ar_stack_peek(ar_stack *stack) {
  if (ar_stack_empty(stack)) {
    fprintf(stderr, "Stack underflow.\n");
    exit(1);
  }
  ar_stack_item item = stack->items[stack->num_items - 1];
  return item;
}

ar_stack_item ar_stack_pop(ar_stack *stack) {
  ar_stack_item item = ar_stack_peek(stack);
  stack->items[stack->num_items - 1] = NULL;
  stack->num_items--;
  return item;
}

