
#ifndef TUPLES_H_INCLUDED
#define TUPLES_H_INCLUDED

struct tuple {
  void *first;
  void *second;
};

struct triple {
  void *first;
  void *second;
  void *third;
};

typedef struct tuple tuple;
typedef struct triple triple;

#endif
