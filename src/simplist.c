
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "fail.h"
#include "simplist.h"

struct simplist {
  int span;
  int capacity;
  simplist_item *items;
};

simplist *make_simplist(int initial_capacity) {
  simplist *list = malloc(sizeof(simplist));
  list->span = 0;
  list->capacity = initial_capacity;
  list->items = malloc(list->capacity * sizeof(simplist_item));
  for (int i=0; i<list->capacity; i++)
    list->items[i] = NULL;
  return list;
}

void free_simplist(simplist *list) {
  free(list->items);
  free(list);
}

static void extend_simplist(simplist *list) {
  list->capacity = list->capacity * 3 / 2;
  simplist_item *temp = realloc(list->items, list->capacity * sizeof(simplist_item));
  if (temp == NULL) fail("Realloc failed.\n");
  list->items = temp;
  for (int i=list->span; i<list->capacity; i++)
    list->items[i] = NULL;
}

bool simplist_empty(simplist *list) {
  for (int i=0; i<list->capacity; i++)
    if (list->items[i] != NULL) return false;
  return true;
}

static bool simplist_full(simplist *list) {
  return (list->span == list->capacity);
}

int simplist_add(simplist *list, simplist_item item) {
  int i;
  for (i=0; i<list->capacity; i++) {
    if (list->items[i] == NULL) {
      list->items[i] = item;
      break;
    }
  }
  list->span++;
  if (simplist_full(list)) extend_simplist(list);
  return i;
}

void simplist_delete_all(simplist *list) {
  for (int i=0; i<list->span; i++)
    list->items[i] = NULL;
}

int simplist_length(simplist *list) {
  int length = 0;
  for (int i=0; i<list->span; i++)
    if (list->items[i] != NULL) length++;
  return length;
}

simplist_item simplist_find(simplist *list, simplist_item item1,
			  bool (*comparator)(simplist_item, simplist_item)) {
  int index = simplist_index(list, item1, comparator);
  if (index == -1) return NULL;
  else return list->items[index];
}

bool simplist_contains(simplist *list, simplist_item item1,
		      bool (*comparator)(simplist_item, simplist_item)) {
  if (simplist_find(list, item1, comparator) == NULL)
    return false;
  else
    return true;
}

void simplist_delete(simplist *list, simplist_item item1,
		    bool (*comparator)(simplist_item, simplist_item)) {
  int index = simplist_index(list, item1, comparator);
  if (index == -1) fail("Item not found.\n");
  else list->items[index] = NULL;
}

int simplist_add_new(simplist *list, simplist_item item,
		    bool (*comparator)(simplist_item, simplist_item)) {
  if (simplist_contains(list, item, comparator))
    return -1;
  else
    return simplist_add(list, item);
}

int simplist_index(simplist *list, simplist_item item1,
		  bool (*comparator)(simplist_item, simplist_item)) {
  for (int i=0; i<list->span; i++) {
    if ((list->items[i] != NULL) && (*comparator)(item1, list->items[i])) {
      return i;
    }
  }
  return -1;
}

static void check_bounds(simplist *list, int index) {
  if ((index < 0) || (index >= list->span))
    fail("Index out of bounds.\n");
}

void simplist_delete_at(simplist *list, int index) {
  check_bounds(list, index);
  if (list->items[index] == NULL)
    fail("No item to delete at index %d.\n", index);
  list->items[index] = NULL;
}

void simplist_set_at(simplist *list, int index, simplist_item new_item) {
  check_bounds(list, index);
  list->items[index] = new_item;
}

simplist_item simplist_get_at(simplist *list, int index) {
  check_bounds(list, index);
  return list->items[index];
}

void simplist_mapc(simplist *list, void *env,
		  void (*function)(void*, simplist_item)) {
  for (int i=0; i<list->span; i++)
    if (list->items[i] != NULL) (*function)(env, list->items[i]);
}
