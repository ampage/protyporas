
#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "fail.h"
#include "sl-list.h"

struct cell;
typedef struct cell cell;

struct field;
typedef struct field field;

union occupant;
typedef union occupant occupant;

struct sl_list {
  int length;
  cell *first;
  cell *last;
};

struct cell {
  field *car;
  field *cdr;
};

struct field {
  bool is_item;
  occupant *occupant;
};

union occupant {
  cell *cell;
  sl_list_item item;
};

// Some functions (such as mapc or the printing functions) could be simplified
// now that I've decided to support only flat, proper lists containing no
// NULLs as items, but the more comprehensive versions are useful for reference.

static field *make_cell_field(cell *c) {
  field *f = malloc(sizeof(field));
  f->is_item = false;
  f->occupant = malloc(sizeof(occupant));
  f->occupant->cell = c;
  return f;
}

static field *make_item_field(sl_list_item item) {
  field *f = malloc(sizeof(field));
  f->is_item = true;
  f->occupant = malloc(sizeof(occupant));
  f->occupant->item = item;
  return f;
}

static cell *cons(field *f1, cell *c1) {
  cell *c2 = malloc(sizeof(cell));
  c2->car = f1;
  if (c1 == NULL)
    c2->cdr = NULL;
  else
    c2->cdr = make_cell_field(c1);
  return c2;
}

static cell *cons_item(sl_list_item item, cell *c1) {
  return cons(make_item_field(item), c1);
}

/*
static cell *cons_field(field *f1, field *f2) {
  cell *c1 = malloc(sizeof(cell));
  c1->car = f1;
  c1->cdr = f2;
  return c1;
}
*/

static sl_list *create_aux(int num_elts, va_list elts) {
  sl_list_item items[num_elts];
  for (int i=num_elts-1; i>=0; i--) {
    items[i] = va_arg(elts, sl_list_item);
    assert_non_null(items[i]);
  }
  sl_list *list = malloc(sizeof(sl_list));
  list->length = num_elts;
  cell *c = cons_item(items[0], NULL);
  list->last = c;
  for (int j=1; j<num_elts; j++)
    c = cons_item(items[j], c);
  list->first = c;
  return list;
}

// Refactor these two functions
sl_list *sl_list_create(int num_elts, ...) {
  sl_list *list;
  if (num_elts < 0)
    fail("Lists must be of length 0 or greater.\n");
  else if (num_elts == 0) {
    list = malloc(sizeof(sl_list));
    list->length = 0;
    list->first = NULL;
    list->last = NULL;
    return list;
  }
  va_list elts;
  va_start(elts, num_elts);
  list = create_aux(num_elts, elts);
  va_end(elts);
  return list;
}

sl_list *sl_list_vcreate(int num_elts, va_list elts) {
  sl_list *list;
  if (num_elts < 0)
    fail("Lists must be of length 0 or greater.\n");
  else if (num_elts == 0) {
    list = malloc(sizeof(sl_list));
    list->length = 0;
    list->first = NULL;
    list->last = NULL;
    return list;
  }
  list = create_aux(num_elts, elts);
  va_end(elts);
  return list;
}

static void free_cell(context env, sl_list_item_action item_freer,
		      cell *c1) {
  if (c1 != NULL) {
    if (c1->car != NULL) {
      if (c1->car->is_item) {
	item_freer(env, c1->car->occupant->item);
	free(c1->car->occupant);
	free(c1->car);
      }
      else {
	free_cell(env, item_freer, c1->car->occupant->cell);
	free(c1->car->occupant);
	free(c1->car);
      }
    }
    if (c1->cdr != NULL) {
      if (c1->cdr->is_item) {
	item_freer(env, c1->cdr->occupant->item);
	free(c1->cdr->occupant);
	free(c1->cdr);
      }
      else {
	free_cell(env, item_freer, c1->cdr->occupant->cell);
	free(c1->cdr->occupant);
	free(c1->cdr);
      }
    }
    free(c1);
  }
}

void sl_list_free(context env, sl_list_item_action item_freer,
		  sl_list *list) {
  assert_non_null(list);
  list->last = NULL;
  free_cell(env, item_freer, list->first);
  list->first = NULL;
  free(list);
}

static void print_cell_aux(context env, sl_list_item_action item_printer,
			   cell *c1) {
  if (c1->car != NULL) {
    if (c1->car->is_item)
      item_printer(env, c1->car->occupant->item);
    else {
      printf("( ");
      print_cell_aux(env, item_printer, c1->car->occupant->cell);
      printf(") ");
    }
  }
  else
    printf("nil ");
  if (c1->cdr != NULL) {
    if (c1->cdr->is_item) {
      printf(". ");
      item_printer(env, c1->cdr->occupant->item);
    }
    else
      print_cell_aux(env, item_printer, c1->cdr->occupant->cell);
  }
}

static void print_cell(context env, sl_list_item_action item_printer,
		       cell *c1) {
  if (c1 == NULL)
    printf("nil ");
  else {
    printf("( ");
    print_cell_aux(env, item_printer, c1);
    printf(")");
  }
  printf("\n");
}

void sl_list_print(context env, sl_list_item_action item_printer,
		   sl_list *list) {
  assert_non_null(list);
  print_cell(env, item_printer, list->first);
}

bool sl_list_empty(sl_list *list) {
  assert_non_null(list);
  return (list->first == NULL);
}

sl_list_item sl_list_first_item(sl_list *list) {
  assert_non_null(list);
  if (list->first == NULL)
    return NULL;
  else
    return list->first->car->occupant->item;
}

sl_list_item sl_list_last_item(sl_list *list) {
  assert_non_null(list);
  if (list->last == NULL)
    return NULL;
  else
    return list->last->car->occupant->item;
}

static cell *last_cell(cell *c1) {
  cell *cn = c1;
  while (cn->cdr != NULL)
    cn = cn->cdr->occupant->cell;
  return cn;
}

int sl_list_length(sl_list *list) {
  assert_non_null(list);
  return list->length;
}

sl_list *sl_list_cons(sl_list_item item, sl_list *list) {
  assert_non_null(list);
  list->first = cons_item(item, list->first);
  list->length += 1;
  return list;
}

sl_list *sl_list_cons_(context env, sl_list_item_transformer item_copier,
		       sl_list_item item, sl_list *list) {
  return sl_list_cons(item_copier(env, item), list);
}

void sl_list_mapc(context env, sl_list_item_action action,
		  sl_list *list) {
  assert_non_null(list);
  if (list->first != NULL) {
    cell *cn = list->first;
    while (true) {
      if (cn->car != NULL) {
	if (!cn->car->is_item)
	  fail("Nested lists not supported.\n");
	action(env, cn->car->occupant->item);
      }
      if (cn->cdr != NULL)
	if (cn->cdr->is_item)
	  fail("Cannot map a function over an improper list.\n");
        else
	  cn = cn->cdr->occupant->cell;
      else
	break;
    }
  }
}

// Because we don't have GC, and we want the user to only see the sl_list type
// (not the cells or fields), the API should not provide any functions that
// would require a throwaway call to malloc(sizeof(sl_list)), which, unless the
// user is extremely careful, would cause memory leaks. That means: no showing
// subsets of the whole list. You can cons things onto it, but not call cdr, for
// example. Right? This also means that I cannot use the iterator pattern:
// calling next() would give you an intermediate value, which would have to be
// a cell, not an sl_list. We need the latter type to support O(1) append and
// length operations.

// For now, assume that the user wants to keep the old list too. Write
// alternate versions that mutate the old list, freeing up anything that needs
// freeing?

static cell *map_aux(context env, sl_list_item_transformer func, cell *xs) {
  if (xs == NULL)
    return NULL;
  if (xs->cdr == NULL)
    return cons_item(func(env, xs->car->occupant->item),
		     map_aux(env, func, NULL));
  else
    return cons_item(func(env, xs->car->occupant->item),
		     map_aux(env, func, xs->cdr->occupant->cell));
}

sl_list *sl_list_map(context env, sl_list_item_transformer transformer,
		     sl_list *list) {
  sl_list *result = sl_list_create(0);
  result->length = list->length;
  result->first = map_aux(env, transformer, list->first);
  result->last = last_cell(result->first);
  return result;
}

static bool equal_aux(context env, sl_list_item_equator equal,
		      cell *c1, cell *c2) {
  if (c1->cdr == NULL)
    return (c2->cdr == NULL);
  else if (c2->cdr == NULL) return false;
  else {
    if (equal(env, c1->car->occupant->item, c2->car->occupant->item))
      return equal_aux(env, equal, c1->cdr->occupant->cell,
		       c1->cdr->occupant->cell);
    else return false;
  }
}

bool sl_list_equal(context env, sl_list_item_equator equal,
		   sl_list *list1, sl_list *list2) {
  assert_non_null(list1);
  assert_non_null(list2);
  if (list1->first == NULL)
    return (list2->first == NULL);
  else if (list2->first == NULL) return false;
  return equal_aux(env, equal, list1->first, list2->first);
}

static void *foldr_aux(context env, sl_list_item_combiner combine,
		       void *initial_value, cell *c1) {
  if (c1 == NULL)
    return initial_value;
  if (c1->cdr == NULL)
    return combine(env, c1->car, foldr_aux(env, combine, initial_value, NULL));
  else
    return combine(env, c1->car, foldr_aux(env, combine, initial_value,
					   c1->cdr->occupant->cell));
}

void *sl_list_foldr(context env, sl_list_item_combiner combine,
		    void *initial_value, sl_list *list) {
  assert_non_null(list);
  return foldr_aux(env, combine, initial_value, list->first);
}

static int calculate_length(sl_list *list) {
  assert_non_null(list);
  if (list->first == NULL)
    return 0;
  int i = 1;
  cell *cn = list->first;
  while (cn->cdr != NULL) {
    cn = cn->cdr->occupant->cell;
    i++;
  }
  return i;
}

static cell *filter_aux(context env, sl_list_item_predicate test, cell *xs) {
  if (xs == NULL)
    return NULL;
  if (xs->cdr == NULL) {
    if (test(env, xs->car->occupant->item))
        return cons_item(xs->car->occupant->item,
			 filter_aux(env, test, NULL));
    else
      return filter_aux(env, test, NULL);
  }
  else {
    if (test(env, xs->car->occupant->item))
      return cons_item(xs->car->occupant->item,
		       filter_aux(env, test, xs->cdr->occupant->cell));
    else
      return filter_aux(env, test, xs->cdr->occupant->cell);
  }
}

// This could be more efficient.
sl_list *sl_list_filter(context env, sl_list_item_predicate test,
			sl_list *list) {
  sl_list *result = sl_list_create(0);
  result->first = filter_aux(env, test, list->first);
  result->last = last_cell(result->first);
  result->length = calculate_length(result);
  return result;
}

// Be sure to save a pointer to the list's first cell before calling this.
static void free_wrapper_only(sl_list *list) {
  assert_non_null(list);
  list->last = NULL;
  list->first = NULL;
  free(list);
}

sl_list *sl_list_nconc1(sl_list *list, sl_list_item item) {
  cell *c1 = cons_item(item, NULL);
  if (list->last == NULL)
    list->last = c1;
  else {
    list->last->cdr = make_cell_field(c1);
    list->last = c1;
  }
  if (list->first == NULL) list->first = list->last;
  list->length++;
  return list;
}

sl_list *sl_list_nconc(sl_list *list1, sl_list *list2) {
  assert_non_null(list1);
  assert_non_null(list2);
  list1->length = list1->length + list2->length;
  if (list2->first == NULL) {
    free_wrapper_only(list2);
    return list1;
  }
  if (list1->first == NULL) {
    list1->first = list2->first;
    list1->last = list2->last;
    free_wrapper_only(list2);
    return list1;
  }
  list1->last->cdr = make_cell_field(list2->first);
  list1->last = list2->last;
  free_wrapper_only(list2);
  return list1;
}
