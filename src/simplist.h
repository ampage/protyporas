/*
A "simplist" is a custom data structure designed to be both flexible and as
simple as possible to implement, at the expense of the user having to know
roughly how it works in order to use it optimally.

It's a dynamic, generic array list that works as follows:

By default, elements are always added in the leftmost available space. So if
you only ever add elements to it (never remove any), element order is
preserved, so you can access elements by position as you would in, say, a
Python list.

You may violate this rule by directly setting the value at a specified index,
but if you're not careful, this may either introduce duplicates, or, if you set
it to NULL, create a new leftmost gap in the middle of the array, meaning that
later additions may upset the order of elements. These may or may not be
problematic depending on your use case.

The rule of thumb is: if you already know the position of an element, you can
get/set/delete it by index (O(1)), but otherwise you must traverse the list to
locate it first (O(n))*.

Elements are actually void pointers, so client code must cast them before de-
referencing them.

To support this, some API functions are "higher-order": they take function
pointers as arguments, occasionally along with an "environment" argument for
providing limited closure-like behaviour. (This may be ignored if not needed.)
If you need to pass more than one thing in the environment argument, use the
provided tuple/triple struct types. If you need to pass more than three
things, you'll have to define your own struct type for doing so.

If you want to avoid duplicates, only use simplist_add_new() to add elements,
and be careful with simplist_set_at().

Big-Oh complexity of each function is given in its description.

*/

// Rename comparator to equality-test where appropriate, since after adding
// binary search, will need to distinguish between the two.

// TODO: -update everything below to reflect naming/conceptual changes
//       -maybe rethink failure behaviour of functions that can fail
//       -add Big-Oh stuff

//*BRAINWAVE: could I perform binary search here instead, so long as the items
//are actually orderable (like integers)? See Conor's iterative implementation.
//Could revert to linear search only when no alternative. The user will know
//whether to call the binary search one or linear search one by whether they
//can meaningfully define a comparator, or just an equality predicate.

// YES: need two versions of many functions: one performs linear search given
// an equality predicate, the other performs binary search given a comparator.

#ifndef SIMPLIST_H_INCLUDED
#define SIMPLIST_H_INCLUDED

#include <stdbool.h>

typedef void* simplist_item;

struct simplist;
typedef struct simplist simplist;

simplist *make_simplist(int initial_capacity);

void free_simplist(simplist *list);

// Checks that all elements are null.
bool simplist_empty(simplist *list);

// Inserts item into the leftmost null slot, expanding the array if needed.
// Returns the index of the new item, in case it is needed.
int simplist_add(simplist *list, simplist_item item);

// Sets all elements to null, regardless of their prior values. Never fails.
void simplist_delete_all(simplist *list);

// Returns the number of non-null list elements.
int simplist_length(simplist *list);

// Returns the element in the list that matches the item via the comparator.
// Must pass it a comparator which coerces the void pointers to the actual
// type of data they point to, dereferences them, and compares those values.
// If it is not found, returns null.
simplist_item simplist_find(simplist *list, simplist_item item1,
			  bool (*comparator)(simplist_item, simplist_item));

// Returns true if the item is found within the list.
bool simplist_contains(simplist *list, simplist_item item1,
		      bool (*comparator)(simplist_item, simplist_item));

// Deletes the given element from the list, if present. It not, this fails.
void simplist_delete(simplist *list, simplist_item item1,
		    bool (*comparator)(simplist_item, simplist_item));

// Like add, but uses a comparator to make sure that the item is not already
// in the list. If it is, does nothing and returns -1.
int simplist_add_new(simplist *list, simplist_item item,
		    bool (*comparator)(simplist_item, simplist_item));

// Returns the index of an element, or -1 if not found.
int simplist_index(simplist *list, simplist_item item1,
		  bool (*comparator)(simplist_item, simplist_item));

// Deletes the element at the index provided, if it isn't already null.
// If it is, this fails.
void simplist_delete_at(simplist *list, int index);

// Sets the value at the index provided.
void simplist_set_at(simplist *list, int index, simplist_item new_item);

// Gets the value at the index provided. Returns null if the value is null.
simplist_item simplist_get_at(simplist *list, int index);

// Map a function over all non-null list elements. To emulate lexical closures
// to a limited extent, accepts an environment argument that must be passed
// on as the first argument to the 'function' argument. For now, cannot return
// a value (so the function may only have side effects), but should implement
// this.
void simplist_mapc(simplist *list, void *env,
		  void (*function)(void*, simplist_item));

#endif
