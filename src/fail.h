
#ifndef FAIL_H_INCLUDED
#define FAIL_H_INCLUDED

#include <stdarg.h>

void fail(const char *fmt, ...);
void assert_non_null(void *thing);
//void assert_non_null(int num_args, ...);

#endif
