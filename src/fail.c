
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>

#include "fail.h"

void fail(const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  vfprintf(stderr, fmt, args);
  exit(1);
  va_end(args);
}

void assert_non_null(void *thing) {
  assert(thing != NULL);
}

/*
void assert_non_null(int num_args, ...) {
  if (num_args <= 0)
    fail("Must provide at least one pointer to check.\n");
  va_list args;
  va_start(args, num_args);
  //void *things[num_args];
  for (int i=0; i<num_args; i++)
    //things[i] = va_arg(args, void*);
    if (va_arg(args, void*) == NULL)
      fail("Argument %d is NULL.\n", i+1);
  va_end(args);
}
*/
