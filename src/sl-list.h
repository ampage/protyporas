/*
A generic singly-linked list implementation with an FP-style interface.

Most API functions take both a function pointer and a "context" (or
"environment") variable, to provide limited closure-like ability. (This solves
the "downwards funarg problem", but not the upwards one. Hence "limited").

Note that due to the absence of GC, the user must be careful to avoid both
memory leaks and double frees. Rules of thumb:

TODO: update this description to reflect the API clarifications descibed
      below the bullet points. Also clarify: do I need copying/non-copying
      versions of (v)create? Should they take a constructor function as an
      argument? Should instead say that if in doubt, use copying versions
      except if constructing elements directly in function call...

-Be aware of which higher-order functions create a new list and which don't.
 If they do, be sure to keep pointer to the old one and free that.

-To sl_list_free, you can pass either a function that actually frees an item,
 or a "no-op" function. Passing the former will free the list along with its
 contents; passing the latter will free only the list itself. Only do the latter
 if you still have pointers somewhere to all of the list's contents.

-When writing a function to pass to an API one, decide whether you want to
 create fresh items to add/collect in the result list, or not, and write it
 accordingly. If in doubt, always create fresh items and free the contents of
 your lists at the same time as you free the lists themselves (i.e. never pass
 a no-op freeing function to sl_list_free).

-If an item exists in more than one list, you must either: free all but the
 final list containing it using a no-op freeing function, then free the final
 one along with its contents; or free all of the lists containing it using
 a no-op freeing function, then free the item directly using some other pointer
 to it. Use these techniques at your discretion: things can get complicated if
 there are some items which exist in multiple lists, but others which don't.
 Use Memcheck to ensure that there is no leakage or double freeing.

Note also that there are two versions of many list-manipulation functions.
The ones ending in an underscore ALWAYS create fresh copies of list items
instead of referencing existing ones. To do this, they take an item-copying
function as an argument, along with a context variable in case the copying
function requires any additional information. Again, if in doubt, always use
the item-copying versions of the API functions; in that way, every list can
be freed independently. If using the other ones, consider the caveats above.

Finally, note that this distinction is NOT the same as the distinction between
destructive and non-destructive list operations. E.g: The nconc function always
destroys its second argument, and always modifies the first; whereas the append
function (which conceptually has the same purpose, namely concatenating two
lists) never destroys or modifies either list. There are nevertheless two
versions of each function: one with a trailing underscore, one without, the
former always copying list elements instead of using them directly.

For this reason, it is recommended that you always rely on the return value of
list-manipulation functions, as opposed to relying on their side-effects alone.
*/

#ifndef SL_LIST_H_INCLUDED
#define SL_LIST_H_INCLUDED

#include <stdbool.h>
#include <stdarg.h>

struct sl_list;
typedef struct sl_list sl_list;

typedef void* sl_list_item;

typedef void* context;

typedef bool (*sl_list_item_predicate)(context, sl_list_item);

typedef void (*sl_list_item_action)(context, sl_list_item);

typedef void *(*sl_list_item_transformer)(context, sl_list_item);

typedef int (*sl_list_item_comparator)(context, sl_list_item, sl_list_item);

typedef bool (*sl_list_item_equator)(context, sl_list_item, sl_list_item);

typedef void *(*sl_list_item_combiner)(context, void*, void*);

sl_list *sl_list_create(int num_elts, ...);

sl_list *sl_list_vcreate(int num_elts, va_list elts);

// item_freer may be a no-op, if you only want to free the list itself.
void sl_list_free(context env, sl_list_item_action item_freer, sl_list *list);

void sl_list_print(context env, sl_list_item_action item_printer,
		   sl_list *list);

bool sl_list_empty(sl_list *list);

sl_list_item sl_list_first_item(sl_list *list);

sl_list_item sl_list_last_item(sl_list *list);

int sl_list_length(sl_list *list);

sl_list *sl_list_cons(sl_list_item item, sl_list *list);
sl_list *sl_list_cons_(context env, sl_list_item_transformer item_copier,
		       sl_list_item item, sl_list *list);

sl_list *sl_list_nconc(sl_list *list1, sl_list *list2);

sl_list *sl_list_nconc1(sl_list *list, sl_list_item item);

// sl_list *sl_list_append(sl_list *list1, sl_list *list2);

// sl_list *sl_list_append1(sl_list *list, sl_list_item item);

void sl_list_mapc(context env, sl_list_item_action action,
		  sl_list *list);

sl_list *sl_list_map(context env, sl_list_item_transformer transformer,
		     sl_list *list);

void *sl_list_foldr(context env, sl_list_item_combiner combine,
		    void *initial_value, sl_list *list);

sl_list *sl_list_filter(context env, sl_list_item_predicate test,
			sl_list *list);

bool sl_list_equal(context env, sl_list_item_equator equal,
		   sl_list *list1, sl_list *list2);

// Stack interface
/*
bool sl_list_empty(sl_list *list);
void sl_list_push(sl_list *list, sl_list_item item);
sl_list_item sl_list_pop(sl_list *list);
sl_list_item sl_list_peek(sl_list *list);
*/

// FIFO queue interface
// enqueue, dequeue, anything else?

#endif
