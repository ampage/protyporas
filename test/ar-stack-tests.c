
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "ar-stack.h"

typedef struct test_item {
  long val;
} itm;

itm *make_itm(long n) {
  itm *i = malloc(sizeof(itm));
  i->val = n;
  return i;
}

/*
static void print_stack(ar_stack *stack) {
  // This violates the decoupling, so should probably move to ar-stack.c
  printf("num_items = %d, capacity = %d, { ",
	 stack->num_items, stack->capacity);
  for (int i = 0; i < stack->num_items; i++) {
    if (stack->items[i] != NULL)
      printf("%ld ", ((itm*) stack->items[i])->val);
    else
      printf("- ");
  }
  printf("}\n");
}
*/

void ar_stack_tests(void) {
  int initial_capacity = 16;
  ar_stack *s1 = make_ar_stack(initial_capacity);
  ar_stack_item i1 = make_itm(6);
  ar_stack_item i2 = make_itm(4);
  ar_stack_item i3 = make_itm(7);
  ar_stack_item i4 = make_itm(9);
  ar_stack_item i5 = make_itm(8);
  ar_stack_item i6 = make_itm(1);
  
  assert(ar_stack_empty(s1));
  //assert(!full(s1)); // don't want this in API, but I DO want it in tests...?
  ar_stack_push(s1, i1);
  assert(!ar_stack_empty(s1));
  assert((i1 == ar_stack_peek(s1)));
  assert((i1 == ar_stack_pop(s1)));
  assert(ar_stack_empty(s1));

  ar_stack_push(s1, i2);
  ar_stack_push(s1, i3);
  ar_stack_push(s1, i4);
  ar_stack_push(s1, i5);
  ar_stack_push(s1, i6);
 
  assert((i6 == ar_stack_pop(s1)));
  assert((i5 == ar_stack_pop(s1)));
  assert((i4 == ar_stack_pop(s1)));
  assert((i3 == ar_stack_pop(s1)));
  assert((i2 == ar_stack_pop(s1)));

  assert(ar_stack_empty(s1));

  ar_stack_push(s1, i2);
  for (int j=0; j < initial_capacity; j++)
    ar_stack_push(s1, i3);
  for (int k=0; k < initial_capacity; k++)
    ar_stack_pop(s1);
  assert((i2 == ar_stack_pop(s1)));
  assert(ar_stack_empty(s1));
  
  free(i1);
  free(i2);
  free(i3);
  free(i4);
  free(i5);
  free(i6);
  free_ar_stack(s1);
  
  printf("Ar-stack tests passed.\n");
}
