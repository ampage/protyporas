
void simplist_tests(void);
void ar_stack_tests(void);
void sl_list_tests(void);

int main(void) {
  simplist_tests();
  ar_stack_tests();
  sl_list_tests();
  return 0;
}
