
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "simplist.h"

typedef struct test_item {
  long val;
} itm;

static itm *make_itm(long n) {
  itm *i = malloc(sizeof(itm));
  i->val = n;
  return i;
}

static bool itm_comparator(simplist_item item1, simplist_item item2) {
  itm *x = item1;
  itm *y = item2;
  return (x->val == y->val);
}

/*
static void itm_printer(void *ignore, simplist_item item) {
  printf("%ld ", ((itm*) item)->val);
}

static void print_list(simplist *list) {
  // This would require the struct definition of simplist to be extern'd
  //printf("span = %d, capacity = %d, { ", list->span, list->capacity);
  printf("{ ");
  simplist_mapc(list, NULL, itm_printer);
  printf("}\n");
}
*/

void simplist_tests(void) {
  int initial_capacity = 16;
  simplist *l1 = make_simplist(initial_capacity);
  simplist_item i1 = make_itm(6);
  simplist_item i2 = make_itm(4);
  simplist_item i3 = make_itm(7);
  simplist_item i4 = make_itm(9);
  simplist_item i5 = make_itm(8);
  simplist_item i6 = make_itm(1);
  simplist_item i7 = make_itm(5);
  
  //print_list(l1);
  assert(simplist_empty(l1));
  simplist_add_new(l1, i1, itm_comparator);
  //print_list(l1);
  assert(!simplist_empty(l1));
  assert(simplist_contains(l1, i1, itm_comparator));
  assert(!simplist_contains(l1, i7, itm_comparator));
  assert((simplist_index(l1, i1, itm_comparator) == 0));
  simplist_set_at(l1, 0, i7);
  assert(simplist_contains(l1, i7, itm_comparator));
  assert((simplist_add_new(l1, i7, itm_comparator) == -1));
  simplist_delete_at(l1, 0);
  assert(simplist_empty(l1));

  //print_list(l1);
  
  //int index = simplist_add(l1, i2);
  //printf("%d\n", index);
  //print_list(l1);
  simplist_add(l1, i2);
  simplist_add(l1, i3);
  //print_list(l1);
  assert((simplist_index(l1, i3, itm_comparator) == 1));
  assert((simplist_get_at(l1, 1) == i3));
  simplist_add(l1, i4);
  //print_list(l1);
  simplist_add(l1, i5);
  //print_list(l1);
  simplist_add(l1, i6);
  //print_list(l1);

  assert((simplist_find(l1, i3, itm_comparator) != NULL));
  assert(!simplist_contains(l1, i7, itm_comparator));
  simplist_delete(l1, i3, itm_comparator);
  //simplist_delete(l1, i7, itm_comparator); //fails

  for (int j=0; j < initial_capacity; j++)
    simplist_add(l1, i3);

  //print_list(l1);
  assert(simplist_length(l1) == 20);
  simplist_delete_all(l1);
  assert(simplist_empty(l1));
  //print_list(l1);
  
  free(i1);
  free(i2);
  free(i3);
  free(i4);
  free(i5);
  free(i6);
  free(i7);
  free_simplist(l1);
  
  printf("Simplist tests passed.\n");
}

