
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdarg.h>

#include "sl-list.h"

typedef struct test_item {
  long val;
} itm;

static itm *make_itm(long n) {
  itm *i = malloc(sizeof(itm));
  i->val = n;
  return i;
}

static bool itm_equator(context env, sl_list_item item1, sl_list_item item2) {
  itm *x = item1;
  itm *y = item2;
  return (x->val == y->val);
}

static bool itm_eq(context env, sl_list_item item1, sl_list_item item2) {
  itm *x = item1;
  itm *y = item2;
  return (x == y);
}

static void itm_printer(context env, sl_list_item item) {
  itm *x = item;
  printf("%ld ", x->val);
}

static void itm_freer(context env, sl_list_item item) {
  itm *x = item;
  free(x);
}

static void noop_freer(context env, sl_list_item item) {}

static void *length_combiner(context env, void *ignore, void *n) {
  *(int*)n = 1 + *(int*)n;
  return n;
}

static int find_length(sl_list *list) {
  int n = 0;
  return *(int*) sl_list_foldr(NULL, length_combiner, &n, list);
}

static void *minus1(context env, sl_list_item item) {
  itm *x = item;
  return make_itm(x->val - 1);
}

static sl_list *decrement_all(sl_list *list) {
  return sl_list_map(NULL, minus1, list);
}

static bool is_even(context env, sl_list_item item) {
  itm *x = item;
  return ((x->val % 2) == 0);
}

static sl_list *evens_only(sl_list *list) {
  return sl_list_filter(NULL, is_even, list);
}

/*
static void print_itm_list(sl_list *list) {
  itm *first = sl_list_first_item(list);
  itm *last = sl_list_last_item(list);
  if (first != NULL && last != NULL) {
    printf("length %d first %ld last %ld ", sl_list_length(list),
	   first->val, last->val);
  }
  else
    printf("length %d ", sl_list_length(list));
  sl_list_print(NULL, itm_printer, list);
}
*/

static bool itm_list_equal(sl_list *list1, sl_list *list2) {
  return sl_list_equal(NULL, itm_equator, list1, list2);
}

static void itm_list_expect(sl_list *list1, int num_elts, ...) {
  va_list elts;
  va_start(elts, num_elts);
  sl_list *list2 = sl_list_vcreate(num_elts, elts);
  bool result;
  result = itm_list_equal(list1, list2);
  sl_list_free(NULL, itm_freer, list2);
  assert(result);
}

static void *identity(context env, sl_list_item item) {
  return item;
}

static void *itm_copier(context env, sl_list_item item) {
  itm *x = item;
  return make_itm(x->val);
}

static sl_list *shallow_copy(sl_list *list) {
  return sl_list_map(NULL, identity, list);
}

static sl_list *deep_copy(sl_list *list) {
  return sl_list_map(NULL, itm_copier, list);
}

// Ok, so I think I've figured one thing out. If an itm is a member of two lists,
// there is no way to free everything without an "invalid" free, even if you
// keep an extra pointer to it, free the lists themselves, then free it. Even
// though, in the end, there is no memory leak, because everything has been
// freed, just maybe not in an order the Valgrind likes... Solution? How does this
// differ from in dipsl?

// The problem is that I'm freeing several things which have reciprocal relation-
// ships, so when freeing the second one, it will try to free the first one, (or
// some of its contents) which has/have already been freed.

// Solutions: 1. After freeing sth, set the pointer to NULL, and before freeing
// sth, ensure the pointer is not NULL. This will not work if the two things
// have separate pointers to the same memory. So the better solution: 2. copy
// everything. So an itm cannot exist in two different lists at once. But this
// is a semantic problem w.r.t. the API. E.g. in cons and nconc1, and the stack/
// queue equivalents, you kind of expect it to be the exact same object, not a
// copy.

// So: change all applicable API functions to take a functional argument that
// must create fresh itms. With map, can use the existing functional argument.
// With cons and nconc1, must introduce one. What about nconc, append...?
// sl_list_item_copier?

// Could even have versions of the API functions which are designed to copy
// the original element, add it to the list, then free the original? Bad idea:
// the user will still have a pointer to the freed memory. (But this is the
// case for i1-i6 also. Should set those pointers to NULL after the call to
// sl_list_free(...itm_freer...)).

// map, filter and nconc1 create fresh cells.

// "map minus1" creates fresh itms. "filter is_even" does not.
// "nconc1 i1" does not. "cons" does not.

// (foldr we don't have to worry about.)

void sl_list_tests(void) {
  sl_list_item i1 = make_itm(7);
  sl_list_item i2 = make_itm(9);
  sl_list_item i3 = make_itm(4);
  sl_list_item i4 = make_itm(6);
  sl_list_item i5 = make_itm(8);
  sl_list_item i6 = make_itm(3);

  // create, first/last_item
  sl_list *list1 = sl_list_create(3, i1, i2, i3);
  assert(*(long*)sl_list_first_item(list1) == 7);
  assert(*(long*)sl_list_last_item(list1) == 4);
  
  // print and mapc
  //sl_list_print(NULL, itm_printer, list1);
  //sl_list_mapc(NULL, itm_printer, list1);
  //printf("\n");

  // cons, list_equal
  sl_list *list2 = sl_list_create(2, i2, i3);
  list2 = sl_list_cons(i1, list2);
  assert(itm_list_equal(list1, list2));
  // foldr, empty, length
  assert(find_length(list1) == sl_list_length(list1));
  assert(find_length(list2) == sl_list_length(list2)); 
  sl_list *list3 = sl_list_create(0);
  assert(sl_list_empty(list3));
  assert(!itm_list_equal(list1, list3));
  assert(find_length(list3) == sl_list_length(list3));
  
  // nconc1
  list3 = sl_list_nconc1(list3, make_itm(7));
  assert(*(long*)sl_list_last_item(list3) == 7);
  list3 = sl_list_nconc1(list3, make_itm(9));
  // vcreate
  itm_list_expect(list3, 2, make_itm(7), make_itm(9));

  // map
  sl_list *list4 = sl_list_create(3, i4, i5, i6);
  sl_list *list5 = decrement_all(list1);
  assert(itm_list_equal(list4, list5));

  // filter
  sl_list *list6 = evens_only(list1);
  itm_list_expect(list6, 1, make_itm(4));

  // itm_eq
  assert(itm_eq(NULL, i1, i1));

  // shallow copy
  sl_list *list7 = shallow_copy(list1);
  assert(sl_list_equal(NULL, itm_eq, list1, list7));

  // deep copy
  sl_list *list8 = deep_copy(list1);
  assert(!sl_list_equal(NULL, itm_eq, list1, list8));
  assert(itm_list_equal(list1, list8));

  // free
  sl_list_free(NULL, noop_freer, list2);
  sl_list_free(NULL, itm_freer, list3);
  sl_list_free(NULL, noop_freer, list7);
  sl_list_free(NULL, itm_freer, list1);
  sl_list_free(NULL, itm_freer, list8);
  sl_list_free(NULL, itm_freer, list5);
  sl_list_free(NULL, noop_freer, list6);
  sl_list_free(NULL, itm_freer, list4);
  
  // nconc
  sl_list *list9 = sl_list_create(2, make_itm(1), make_itm(2));
  sl_list *list10 = sl_list_create(2, make_itm(3), make_itm(4));
  sl_list *list11 = sl_list_create(0);
  sl_list *list12 = sl_list_create(0);
  sl_list *list13 = sl_list_create(0);
  sl_list *list14 = sl_list_create(0);
  // For now, assume the second argument will be destroyed unless it's an
  // empty list. Should probably change this so that it is always
  // destroyed, to reduce chances of a leak.
  sl_list_nconc(list9, list13);
  list13 = NULL;
  itm_list_expect(list9, 2, make_itm(1), make_itm(2));
  sl_list_nconc(list11, list9);
  list9 = NULL;
  itm_list_expect(list11, 2, make_itm(1), make_itm(2));
  sl_list_nconc(list12, list14);
  list14 = NULL;
  itm_list_expect(list12, 0);
  sl_list_nconc(list11, list10);
  list10 = NULL;
  itm_list_expect(list11, 4, make_itm(1), make_itm(2), make_itm(3), make_itm(4));

  // more frees
  sl_list_free(NULL, itm_freer, list11);
  sl_list_free(NULL, itm_freer, list12);

  printf("Sl-list tests passed.\n");
}
